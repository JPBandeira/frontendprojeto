import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { RouterModule, Routes } from '@angular/router'

import { InicioComponent } from './model/inicio.component';
import { RaizComponent } from './model/raiz.component';
import { CategoriaComponent } from './Model/categoria.component';


const appRouter: Routes = [
  
  { path: '', component: InicioComponent},
  { path: 'categoria', component: CategoriaComponent},

];


@NgModule({
  declarations: [
    InicioComponent,
    RaizComponent,
    CategoriaComponent,

  ],
  imports: [
    RouterModule.forRoot (
      appRouter , { enableTracing: true }
    ),
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [RaizComponent]
})

export class AppModule { }
