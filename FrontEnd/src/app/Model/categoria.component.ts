import { Component, OnInit } from '@angular/core';
import { Categoria } from '../view/categoria.modelo'

@Component({
  selector: 'app-categoria',
  templateUrl: '../view/categoria.component.html',
  styleUrls: ['../view/categoria.component.css']
})
export class CategoriaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  titulo : string = 'Cadastro de Categorias'
  categoria: Categoria = new Categoria();
  vetorDeCategorias: Categoria[] = [];

  cadastrar() : void {
  	if(this.categoria.Id === undefined){
  		let novaCat: Categoria = new Categoria();
  		novaCat.Id = this.vetorDeCategorias.length + 1;
      novaCat.Nome = this.categoria.Nome;
      novaCat.Responsavel = this.categoria.Responsavel;
  		this.vetorDeCategorias.push(novaCat);
  		console.log(novaCat);
  	} else {
  		let cat = this.vetorDeCategorias.find(c => c.Id == this.categoria.Id);
      cat.Nome = this.categoria.Nome;
      cat.Responsavel = this.categoria.Responsavel;
  	}
  	this.categoria = new Categoria();
  }

  remover(i) : void {
  	let resposta = prompt('Você tem certeza que deseja remover?', 'Responda sim ou não');
  	if(resposta === 'Yes' || resposta === 'yes') {
  		this.vetorDeCategorias.splice(i,1);
  	}
  }

  editar(i):void {
    this.categoria.Nome = this.vetorDeCategorias[i].Nome;
    this.categoria.Id = this.vetorDeCategorias[i].Id;
    this.categoria.Responsavel = this.vetorDeCategorias[i].Responsavel;

  }

}
